/***********************************
 *Book: Computer Programming: 1
 *Chapter 1: First Program
 *Example: 1.1
 *Author: Md Akil Mahmod Tipu
 *Email: amtipu.bb@gmail.com
 *Say Hello to the World*

 ***********************************/
#include <stdio.h>

int main()
{
    printf("Hello World");

    return 0;
}
