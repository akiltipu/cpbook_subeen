/***********************************
 *Computer Programming: Part-1
 *Chapter 4: LOOP
 *Example: 4.2
 *Author: Md Akil Mahmod Tipu
 *Email: amtipu.bb@gmail.com
 
 ***********************************/
#include <stdio.h>
int main ()
{
    int n = 1;
    while(n <= 60){
        printf("%d\n", n);
        n++;
    }

    return 0;
}
